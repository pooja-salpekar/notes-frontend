import React from 'react';
import PropTypes from 'prop-types';
import NewNoteForm from './NewNoteForm'
import './Notes.css';

class CreateNote extends React.Component {
  render() {
    
    if(!this.props.show) {
      return null;
    }

    return (
      <div className="Note-create-backdrop">
        <div className="Note-create-modal">
          < NewNoteForm />
          <div className="Note-footer">
            <button onClick={this.props.onClose}>
              Close
            </button>
          </div>
        </div>
      </div>
    );
  }
}

CreateNote.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool
};

export default CreateNote;
