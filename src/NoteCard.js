import React, { Component } from 'react';
class CreateNote extends Component {    
  render() {
    return (
    	<p className="Note-card" key={this.props.index}>
    		<span className="Note-title">{this.props.note.title}</span>
    		<span className="Note-text">{this.props.note.text}</span>
    	</p> 
    );
  }
}

export default CreateNote;
