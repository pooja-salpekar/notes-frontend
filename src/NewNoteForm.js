import React from 'react';
import './Notes.css';
import 'whatwg-fetch'

class NewNoteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: ''
    };

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTitleChange(event) {
    this.setState({title: event.target.value});
  }

  handleDescriptionChange(event){
    this.setState({description: event.target.value});
  }

  handleSubmit(event) {
    fetch('/notes', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ topic: this.state.title, text: this.state.description})
    });
  }

  render() {
    return (
      <form className="Note-form" onSubmit={this.handleSubmit}>
      <div>
        <label className=".Note-label">Title:</label>
        <input className="Note-form-field" type="text" value={this.state.title} onChange={this.handleTitleChange} />
      </div>
      <div>
        <label className=".Note-label">Description:</label>
        <input className="Note-form-field" type="text" value={this.state.description} onChange={this.handleDescriptionChange} />
      </div>
      <div>
        <input type="submit" value="Submit" />
      </div>
      </form>
    );
  }
}

export default NewNoteForm;
