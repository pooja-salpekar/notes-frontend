import React, { Component } from 'react';
import CreateNote from './CreateNote';
import NoteCard from './NoteCard';
import 'whatwg-fetch'
import './App.css';

class App extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      notes: [],
      isOpen: false
    };
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  componentDidMount() {
    fetch('/notes')
      .then(response => {
        return response.json();
      })
      .then((notes) => {
        this.setState({  notes: notes });
      })
      .catch(error => console.log("error"));
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
        <p className="App-title">Notes-Keeper</p>
        </header>

        <div className="App-intro">
          <button className="Note-create-button" onClick={this.toggleModal}>
            +
          </button>

          <CreateNote show={this.state.isOpen} onClose={this.toggleModal} />
        </div>

        <div className="note-container">
          {this.state.notes.map((note,index) => <NoteCard note={note} index={index}/>)}
        </div>
      </div>
    );
  }
}

export default App;
